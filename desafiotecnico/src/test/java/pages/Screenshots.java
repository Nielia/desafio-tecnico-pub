package pages;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Screenshots {
	

	public static void screenshot(WebDriver driver, String nomeImagem) {
		
		try {
			File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenshot, new File("..\\desafiotecnico\\Screenshots\\".concat(nomeImagem).concat(".png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	
	//private TakesScreenshot driver;
	//TakesScreenshot screenshot = (TakesScreenshot)driver;
	//File source = screenshot.getScreenshotAs(OutputType.FILE);
	
	//FileHandler.copy(source, new File());
	//FileUtils.copyFile(source, new File("c:\\tmp\\screenshot.png"));
	

}
