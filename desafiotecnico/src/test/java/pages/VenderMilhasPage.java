package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class VenderMilhasPage {

	public static WebDriver driver;
	public static By campoQuantidadeMilhas = By
			.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[1]/form/div/div[1]/div/input");
	public static WebDriverWait wait;

	public VenderMilhasPage() {

	}

	public VenderMilhasPage(WebDriver driver) {

		VenderMilhasPage.driver = driver;
		wait = new WebDriverWait(driver, 10);

	}

	public static void fazerVenda() {

		clicarBotaoFacaSuaVenda();
		clicarNaOpcaoCompanhiaAreaAzul();

	}

	public static void informarQtdeDeMilhas(By campoQuantidadeMilhas, WebDriverWait wait, String quantidade) {
		wait.until(ExpectedConditions.presenceOfElementLocated((campoQuantidadeMilhas)));
		driver.findElement((campoQuantidadeMilhas)).clear();
		driver.findElement((campoQuantidadeMilhas)).sendKeys(quantidade);
	}

	private static void clicarNaOpcaoCompanhiaAreaAzul() {
		driver.findElement(By.xpath("//*[@id=\"content1\"]/div[1]/a[1]")).click();
	}

	private static void clicarBotaoFacaSuaVenda() {
		driver.findElement(By.xpath("//*[@id=\"balloon1\"]/button")).click();
	}

}
