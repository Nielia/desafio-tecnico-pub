package test;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import pages.Screenshots;
import pages.VenderMilhasPage;

public class VenderMilhasTest {

	private WebDriver driver = VenderMilhasPage.driver;

	@BeforeClass
	public static void iniciarTest() {

		VenderMilhasPage.fazerVenda();
	}

	@Test
	public void validarQtdeMilhasMenorQuePermitido() {

		VenderMilhasPage.informarQtdeDeMilhas(VenderMilhasPage.campoQuantidadeMilhas, VenderMilhasPage.wait, "350");

		String ofertaMinima = driver
				.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[1]/form/div/div[1]/div/p")).getText();
		assertEquals("A oferta precisa ter no m�nimo 3.500 milhas", ofertaMinima);
		
		Screenshots.screenshot(driver,"validarQtdeMilhasMenorQuePermitido");
	}

	@Test
	public void validarValorAReceber() {

		VenderMilhasPage.informarQtdeDeMilhas(VenderMilhasPage.campoQuantidadeMilhas, VenderMilhasPage.wait, "4000");
		String valorPorMilMilhas = "//*[@id=\"root\"]/div/div/div/div[2]/div[1]/form/div/div[2]/div/input";
		VenderMilhasPage.wait.until(ExpectedConditions.elementToBeClickable(By.xpath(valorPorMilMilhas)));
		driver.findElement(By.xpath(valorPorMilMilhas)).sendKeys("1800");

		String valorAReceber = driver
				.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div[2]/div[1]/form/div/div[6]/div/strong"))
				.getText();
		assertEquals("R$ 72,00", valorAReceber);

		Screenshots.screenshot(driver,"validarValorAReceber");
	}

}
