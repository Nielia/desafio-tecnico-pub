Requisitos da m�quina para rodar o projeto
Java 8, Maven, IDE eclipse, vers�o do navegador Chrome 74.0.3729.169, adicionar o arquivo execut�vel do Chrome driver no diret�rio c:/

- Baixar o projeto na pasta local;
- No Eclipse, Importar o projeto acessando o munu file > import > maven > Existing Maven Projects > next;
- Em root directory clicar em browser, procurar o projeto na pasta local, selecionar o projeto e clicar em finish.
- No projeto importado, acessar a pasta src/test/java > test e no arquivo SuiteTest.java clicar com o botao direito do mouse ir em run as > junit test
- O teste dever� ser executado.

